package thirdapplication.main.second.backtimer;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final int defaultSeconds = 10;
    private int seconds = defaultSeconds;
    private boolean isRunning = false;

    private Button buttonStart;
    private Button buttonPause;
    private Button buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");
            isRunning = savedInstanceState.getBoolean("isRunning");
        }

        initButtons();
        runTimer();
    }

    private void initButtons() {
        buttonStart = findViewById(R.id.buttonStart);
        buttonPause = findViewById(R.id.buttonPause);
        buttonReset = findViewById(R.id.buttonReset);

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // can't start if seconds <= 0
                isRunning = seconds > 0;
            }
        });

        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRunning = false;
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRunning = true;
                seconds = defaultSeconds;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("seconds", seconds);
        outState.putBoolean("isRunning", isRunning);
    }

    private void runTimer() {

        final TextView textSeconds = findViewById(R.id.textSeconds);
        final Handler handler = new Handler();

        handler.post(new Runnable() {
            @Override
            public void run() {
                int hours = seconds / 60 / 60;
                int minutes = seconds % 3600 / 60;
                int sec = seconds % 60;
                String time = String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, sec);
                textSeconds.setText(time);

                if (isRunning) {
                    seconds--;

                    if (sec == 0) {
                        tooltip("Time is over");
                        isRunning = false;
                    }
                }

                handler.postDelayed(this, 1000);
            }
        });
    }

    private void tooltip(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
